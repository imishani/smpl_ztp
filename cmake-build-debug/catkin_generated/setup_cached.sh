#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/itamar/Downloads/smpl_ztp/cmake-build-debug/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/itamar/Downloads/smpl_ztp/cmake-build-debug/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/itamar/Downloads/smpl_ztp/cmake-build-debug/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/itamar/Downloads/smpl_ztp/cmake-build-debug/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH='/home/itamar/Downloads/smpl_ztp:/home/itamar/work/code/ros/assembly_ws/src/leatherman:/home/itamar/work/code/ros/assembly_ws/src/smpl/smpl:/home/itamar/work/code/ros/assembly_ws/src/smpl/smpl_ompl_interface:/home/itamar/work/code/ros/assembly_ws/src/smpl/smpl_ros:/home/itamar/work/code/ros/assembly_ws/src/smpl/sbpl_collision_checking:/home/itamar/work/code/ros/assembly_ws/src/smpl/sbpl_collision_checking_test:/home/itamar/work/code/ros/assembly_ws/src/smpl/smpl_urdf_robot_model:/home/itamar/work/code/ros/assembly_ws/src/smpl/sbpl_kdl_robot_model:/home/itamar/work/code/ros/assembly_ws/src/smpl/sbpl_pr2_robot_model:/home/itamar/work/code/ros/assembly_ws/src/smpl/smpl_test:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/universal_robots:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur10_moveit_config:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur10e_moveit_config:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur16e_moveit_config:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur3_moveit_config:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur3e_moveit_config:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur5_moveit_config:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur5e_moveit_config:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur_description:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur_gazebo:/home/itamar/work/code/ros/assembly_ws/src/universal_robot/ur_kinematics:/opt/ros/noetic/share'